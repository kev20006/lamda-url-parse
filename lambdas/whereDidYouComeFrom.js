// AWS lambda function that checks source and responds accordingly

/**
 * Current implementation is a POST that takes window.location.href as the post body
 */

exports.handler = async (event) => {
    // TODO implement
    const resBody = event.body.includes('localhost') ? 'dev env' : 'prod env';
    const response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
        },
        body: JSON.stringify(resBody)
    };
    return response;
};
