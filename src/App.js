import React, { useState } from 'react';
import './App.css';

function App() {
  const [res, setRes] = useState('');

  const makeRequest = () => {
    
    fetch('https://3gpau780h3.execute-api.eu-west-1.amazonaws.com/default/whereDidYouComeFrom', {
      method: 'POST',
      body: JSON.stringify(document.location.href)
    })
    .then(response => response.json())
    .then(data => setRes(data));
  }
  return (
    <>
      <button onClick={makeRequest}>Make Request</button>
      {res && (
        <div>
          {res}
        </div>)}
    </>
  );
}

export default App;
